package la.stadium;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

public class MyMatchPageActivity extends AppCompatActivity {
    Button ongoing,history;
    RecyclerView on,his;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_match_page);

        ongoing = (Button)findViewById(R.id.btn_ongoing);
        history = (Button)findViewById(R.id.btn_history);

        on = (RecyclerView)findViewById(R.id.ongoing);
        his = (RecyclerView)findViewById(R.id.history);

        ongoing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                his.setVisibility(View.INVISIBLE);
                on.setVisibility(View.VISIBLE);
            }
        });

        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                on.setVisibility(View.INVISIBLE);
                his.setVisibility(View.VISIBLE);
            }
        });


    }
}
