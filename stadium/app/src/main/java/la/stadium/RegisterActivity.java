package la.stadium;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class RegisterActivity extends AppCompatActivity {

    private Button btn_email_register;
    EditText email,password,name;
    private FirebaseAuth mAuth;
    private DatabaseReference user;
    private static final String TAG = "Verify";
    private static final int GALLERY_INTENT = 2;
    FirebaseStorage storage;
    StorageReference mStrorageRef;
    private Uri selectedImage;
    int i;
    ImageView myour;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        btn_email_register = (Button)findViewById(R.id.email_register);
        email = (EditText)findViewById(R.id.email);
        password = (EditText)findViewById(R.id.password);
        name = findViewById(R.id.Name);
        myour = (ImageView)findViewById(R.id.img);
        mAuth = FirebaseAuth.getInstance();
        user = FirebaseDatabase.getInstance().getReference("Register");
        storage = FirebaseStorage.getInstance();
        mStrorageRef = storage.getReference();

        btn_email_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (email.length() == 0 || password.length() == 0)
                    Toast.makeText(RegisterActivity.this, "Please Enter your email & password", Toast.LENGTH_SHORT).show();
                else {
                    createAccount(email.getText().toString(), password.getText().toString());
                    user = FirebaseDatabase.getInstance().getReference("Register").child(Integer.toString(i));
                    user.child("Name").setValue(name.getText().toString());
                    user.child("Email").setValue(email.getText().toString());
                    user.child("Pic").setValue(selectedImage.toString());
                    i++;
                    uploadPic();
                }
            }
        });

        myour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseClick(v);
            }
        });

    }

    public void uploadPic()
    {
        if(selectedImage!=null)
        {
            final ProgressDialog progressDialog = new ProgressDialog(RegisterActivity.this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            StorageReference ref = mStrorageRef.child("PicUser/"+ email.getText().toString());
            ref.putFile(selectedImage)
                    .addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                            progressDialog.dismiss();
                            Toast.makeText(RegisterActivity.this,"Uploaded",Toast.LENGTH_SHORT).show();

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(RegisterActivity.this,"Failed",Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot.getTotalByteCount());
                            progressDialog.setMessage("Uploaded"+(int)progress+"%");
                        }
                    });
        }
    }

    public void chooseClick(View view)
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"Select Picture"),GALLERY_INTENT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == GALLERY_INTENT && resultCode == RESULT_OK && data != null && data.getData() != null)
        {
            selectedImage = data.getData();
            this.myour.setImageURI(selectedImage);
        }
    }

    private void createAccount(String email, String password) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            Toast.makeText(RegisterActivity.this, "COMPLETE", Toast.LENGTH_SHORT).show();

                            sendEmailVerification();




                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(RegisterActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();

                        }

                        // ...
                    }
                });
    }

    @Override
    protected void onStart() {
        super.onStart();
        user.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getChildrenCount()==0)
                {
                    i=1;
                    Log.d("start",Long.toString(dataSnapshot.getChildrenCount()));
                }
                else
                {
                    Log.d("start",Long.toString(dataSnapshot.getChildrenCount()));
                    i = (int)dataSnapshot.getChildrenCount();
                    Log.d("start2",Integer.toString(i));
                    i = i+1;
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void sendEmailVerification() {
        // Disable button

        // Send verification email
        // [START send_email_verification]
        final FirebaseUser user = mAuth.getCurrentUser();
        user.sendEmailVerification()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // [START_EXCLUDE]
                        // Re-enable button
                        //findViewById(R.id.verify_email_button).setEnabled(true);

                        if (task.isSuccessful()) {
                            Toast.makeText(RegisterActivity.this,
                                    "Verification email sent to " + user.getEmail(),
                                    Toast.LENGTH_SHORT).show();
                            mAuth.signOut();

                            Intent intent_regis = new Intent(RegisterActivity.this,LoginPageActivity.class);
                            intent_regis.putExtra("pic",selectedImage.toString());
                            startActivity(intent_regis);
                        } else {
                            Log.e("asd", "sendEmailVerification", task.getException());
                            Toast.makeText(RegisterActivity.this,
                                    "Failed to send verification email.",
                                    Toast.LENGTH_SHORT).show();
                        }
                        // [END_EXCLUDE]
                    }
                });
        // [END send_email_verification]
    }

}
