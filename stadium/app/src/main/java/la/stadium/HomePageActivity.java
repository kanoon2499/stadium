package la.stadium;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

public class HomePageActivity extends AppCompatActivity {
    Button Join_Button,Search_Button,MyMatch_Button,Setting_Button;
    Button btn_facebook_logout;
    private DatabaseReference user;
    private FirebaseAuth mAuth;
    ImageView mpic;
    private StorageReference storageRef;
    private FirebaseStorage mstorage;
    Uri uri;
    private final int CAMERA_REQUEST_CODE = 71;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        Join_Button = (Button)findViewById(R.id.JoinB);
        Search_Button = (Button)findViewById(R.id.SearchB);
        MyMatch_Button =  (Button)findViewById(R.id.MatchB);
        Setting_Button = (Button)findViewById(R.id.SetingB);
        btn_facebook_logout = (Button)findViewById(R.id.facebook_logout);
        mpic = (ImageView)findViewById(R.id.pic);
//        storageRef = mstorage.getReference();
        user = FirebaseDatabase.getInstance().getReference("user");
        storageRef = FirebaseStorage.getInstance().getReference();

        mAuth = FirebaseAuth.getInstance();

        Bundle myBundle = getIntent().getExtras();
        Integer signin = myBundle.getInt("signin");
        FirebaseUser currentUser = mAuth.getCurrentUser();

        if(signin == 1)
        {
            show();
        }
        else if(signin == 2 )
        {
            String pic = myBundle.getString("pic");
            Picasso.with(HomePageActivity.this).load(pic).into(mpic);
        }
        else if(signin == 3) {
            GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(getApplication());
            if (acct != null) {
                String str_photo = acct.getPhotoUrl().toString();
                Picasso.with(HomePageActivity.this).load(str_photo).into(mpic);
            }
        }

        btn_facebook_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user.removeValue();
                mAuth.signOut();
                LoginManager.getInstance().logOut();
                startActivity(new Intent(HomePageActivity.this,LoginPageActivity.class));
                finish();
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        //updateUI(currentUser);
        if(currentUser == null) {
            Toast.makeText(HomePageActivity.this, "You're logout", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(HomePageActivity.this,LoginPageActivity.class));
            finish();
        }
//        String picz = "https://firebasestorage.googleapis.com/v0/b/login-ccee4.appspot.com/o/PicUser%2Fch.noraluk_st%40tni.ac.th?alt=media&token=d5b8a7e3-c186-4a78-b600-4e14dbd7aff2";
//        Glide.with(getApplicationContext()).load(picz)
//                .into(mpic);
    }

    private void show()
    {
        FirebaseUser currentUser = mAuth.getCurrentUser();
        storageRef.child("PicUser/"+currentUser.getEmail()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Log.d("asd0",uri.toString());
                Picasso.with(HomePageActivity.this).load(uri.toString()).into(mpic);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });
    }
}
