package la.stadium;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button mm,st;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mm = (Button)findViewById(R.id.Mymatch);
        st = (Button)findViewById(R.id.setting);

        mm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent HomeIntent = new Intent(MainActivity.this, MyMatchPageActivity.class);
                startActivity(HomeIntent);
            }
        });
        st.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent HomeIntent = new Intent(MainActivity.this, SettingPageActivity.class);
                startActivity(HomeIntent);
            }
        });
    }
}
