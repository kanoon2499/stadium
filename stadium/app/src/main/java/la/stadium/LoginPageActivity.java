package la.stadium;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.shaishavgandhi.loginbuttons.FacebookButton;
import com.shaishavgandhi.loginbuttons.GooglePlusButton;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

public class LoginPageActivity extends AppCompatActivity {

    Button Join_Button,Search_Button,MyMatch_Button,Setting_Button;
    Button btn_email_login, btn_email_register;
    FacebookButton btn_facebook_login;
    EditText email, password;
    URL profile_picture;
    ImageView mImage;

    private FirebaseAuth mAuth;
    private final String TAG = "LOGIN";
    private CallbackManager mCallbackManager;
    private GoogleSignInClient mGoogleSignInClient;
    private final int RC_SIGN_IN = 1;
    GooglePlusButton btn_google_signin;
    private FirebaseUser user;
    private DatabaseReference Duser;
    int i;
    String emailpic;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);

        mAuth = FirebaseAuth.getInstance();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        Duser = FirebaseDatabase.getInstance().getReference("user");


        /**---Map ID---**/
        Join_Button = (Button)findViewById(R.id.JoinB);
        Search_Button = (Button)findViewById(R.id.SearchB);
        MyMatch_Button =  (Button)findViewById(R.id.MatchB);
        Setting_Button = (Button)findViewById(R.id.SetingB);
        btn_email_login = (Button) findViewById(R.id.email_login);
        btn_email_register = (Button) findViewById(R.id.email_register);
        btn_facebook_login =  findViewById(R.id.facebook_login);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        mImage = (ImageView)findViewById(R.id.imageView);
        // Set the dimensions of the sign-in button.
        btn_google_signin = findViewById(R.id.google_signin);
        user = mAuth.getCurrentUser();
        Bundle myBundle = getIntent().getExtras();
//        String asd = myBundle.getString("pic");
//        Picasso.with(LoginPageActivity.this).load(asd).into(mImage);

        btn_email_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (email.length() == 0 || password.length() == 0){
                    Toast.makeText(LoginPageActivity.this, "Please Enter your email & password", Toast.LENGTH_SHORT).show();
                }
                else
                    signIn(email.getText().toString(), password.getText().toString());
            }
        });

        btn_email_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginPageActivity.this,RegisterActivity.class));
            }
        });

        // Initialize Facebook Login button
        mCallbackManager = CallbackManager.Factory.create();

        btn_facebook_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // btn_facebook_login.setEnabled(false);

                LoginManager.getInstance().logInWithReadPermissions(LoginPageActivity.this, Arrays.asList("email", "public_profile"));
                LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.d(TAG, "facebook:onSuccess:" + loginResult);
                        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {;
                                getData(object);
                            }
                        });

                        Bundle  parameters = new Bundle();
                        parameters.putString("fields","name");
                        request.setParameters(parameters);
                        request.executeAsync();
                        handleFacebookAccessToken(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {
                        Log.d(TAG, "facebook:onCancel");
                        // ...
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Log.d(TAG, "facebook:onError", error);
                        // ...
                    }
                });
            }
        });

        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        btn_google_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                google_signIn();
            }
        });


        /**---Set Button Onclick---**/
        Join_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent JoinIntent = new Intent(LoginPageActivity.this, JOIN.class);
//                startActivity(JoinIntent);

            }
        });
        /**---SEARCH ONCLICK---**/
        Search_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent SearchIntent = new Intent(LoginPageActivity.this, SEARCH.class);
//                startActivity(SearchIntent);

            }
        });
        /**---MATCH ONCLICK---**/
        MyMatch_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent MatchIntent = new Intent(LoginPageActivity.this, MATCH.class);
//                startActivity(MatchIntent);

            }
        });
        /**---SETTING ONCLICK---**/
        Setting_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent SettingIntent = new Intent(LoginPageActivity.this, SETTING.class);
//                startActivity(SettingIntent);

            }
        });

    }

    private void getData(JSONObject object) {
        try{
            Duser = FirebaseDatabase.getInstance().getReference("user").child(Integer.toString(i));
            Duser.child("Email").setValue(object.getString("name"));
            profile_picture = new URL("https://graph.facebook.com/"+object.getString("id")+"/picture?width=250&height=250");
            Log.d("pic",profile_picture.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private void google_signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);

        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(getApplication());
        if(acct!=null) {
            Duser = FirebaseDatabase.getInstance().getReference("user").child(Integer.toString(i));
            Duser.child("Email").setValue(acct.getDisplayName());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Pass the activity result back to the Facebook SDK
        mCallbackManager.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
                // ...
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        //updateUI(currentUser);
        if(currentUser != null ) {
            Toast.makeText(LoginPageActivity.this, "You're log in", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(LoginPageActivity.this,HomePageActivity.class));
            finish();
        }
        Duser.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getChildrenCount()==0)
                {
                    i=1;
                    Log.d("start",Long.toString(dataSnapshot.getChildrenCount()));
                }
                else
                {
                    Log.d("start",Long.toString(dataSnapshot.getChildrenCount()));
                    i = (int)dataSnapshot.getChildrenCount();
                    Log.d("start2",Integer.toString(i));
                    i = i+1;
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }



    private void signIn(final String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            if(!user.isEmailVerified())
                            {
                                Toast.makeText(LoginPageActivity.this,"Pls verify email.",Toast.LENGTH_SHORT).show();
                                mAuth.signOut();
                            }
                            else{
                                Toast.makeText(LoginPageActivity.this, "COMPLETE", Toast.LENGTH_SHORT).show();

                                Duser = FirebaseDatabase.getInstance().getReference("user").child(Integer.toString(i));
                                Duser.child("Email").setValue(email);


                                Intent intent_main = new Intent(LoginPageActivity.this,HomePageActivity.class);
                                intent_main.putExtra("signin",1);
                                startActivity(intent_main);
                                finish();
                            }



                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(LoginPageActivity.this, "email failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                        // ...
                    }
                });
    }



    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);
        user = FirebaseAuth.getInstance().getCurrentUser();
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            Toast.makeText(LoginPageActivity.this, "face completed.",
                                    Toast.LENGTH_SHORT).show();
                            btn_facebook_login.setEnabled(true);
                            Intent intent_face = new Intent(LoginPageActivity.this,HomePageActivity.class);
                            intent_face.putExtra("signin",2);
                            intent_face.putExtra("pic",profile_picture.toString());
                            startActivity(intent_face);
                            //   startActivity(new Intent(MainActivity.this,Main2Activity.class));
                            finish();

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(LoginPageActivity.this, "face failed.",
                                    Toast.LENGTH_SHORT).show();
                            btn_facebook_login.setEnabled(true);

                        }

                        // ...
                    }
                }).addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                btn_facebook_login.setEnabled(true);
            }
        });
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            Toast.makeText(LoginPageActivity.this, "google complete.",
                                    Toast.LENGTH_SHORT).show();
                            Intent intent_face = new Intent(LoginPageActivity.this,HomePageActivity.class);
                            intent_face.putExtra("signin",3);
                            startActivity(intent_face);
                            finish();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(LoginPageActivity.this, "google failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                });
    }
}
